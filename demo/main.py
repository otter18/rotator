import inspect
import os
import sys

import cv2
import numpy as np
import streamlit as st

from config import SRC
from utils import check_password

sys.path.append(os.path.join(os.path.dirname(__file__), '..'))

from rotators import Cv2Rotator, ProjectionRotator, TesseractRotator

ROTATORS = [TesseractRotator(), ProjectionRotator(), Cv2Rotator()]


def show_room(obj, img, rotator):
    obj.write(f'### {rotator.md_name}')

    rotated_img, log, orientation, elapsed = rotator.eval_meta(img)

    obj.write(f":gray[Calculations took - {1000 * elapsed:.0f} ms]")
    obj.write(f'Detected orientation - {orientation}')

    with obj.expander("Method description & code"):
        st.write(rotator.description)
        st.write('#### Code')
        st.code(inspect.getsource(rotator.eval))

    obj.write('Result')
    obj.image(rotated_img, use_column_width=True)

    with obj.expander("Log", expanded=True):
        if type(log) is np.ndarray:
            st.image(log)
        else:
            st.write(log)


def main():
    file = None

    def update_query_params():
        st.experimental_set_query_params(img=file)

    st.title('Rotator demo')
    st.write(f":gray[Автоматическое определение ориентации сканов печатных документов]")
    st.write('---')

    info, control, preview = st.columns([3, 3, 2])

    info.write(
        '### Инструкция\n'
        '- Выберите или загрузите картинку\n'
        '- Поворачивайте картинку с помощью ползунка, чтобы изменять входные данные\n'
        '- Посмотрите на результаты разных методов и их описание\n'
        '### Исходный код\n'
        'https://gitlab.com/otter18/rotator\n'
    )

    control.write('### Выбор файла')
    files_list = os.listdir(os.path.join(SRC, '0'))
    selected_img = st.experimental_get_query_params().get('img')
    index = files_list.index(selected_img[0]) if selected_img and selected_img[0] in files_list else 0
    file = control.selectbox(
        label="Select file",
        options=files_list,
        index=index,
        on_change=update_query_params
    )
    uploaded_file = control.file_uploader("Or choose an image", type=['jpg', 'png'])
    angel = control.slider('Rotation', 0, 270, step=90)

    if uploaded_file is None:
        path = os.path.join(SRC, str(angel))
        img = cv2.imread(os.path.join(path, file))
    else:
        file_bytes = np.asarray(bytearray(uploaded_file.read()), dtype=np.uint8)
        img = cv2.imdecode(file_bytes, 1)
        if angel:
            img = cv2.rotate(img, angel // 90 - 1)

    img = cv2.cvtColor(img, cv2.COLOR_BGR2RGB)

    preview.write('### Исходный файл')
    preview.image(img)

    st.write('---')
    st.write('### Результат')
    st.write(f":gray[Решение двумя разными методами]")
    st.write('---')

    cols = st.columns(len(ROTATORS))
    for rotator, col in zip(ROTATORS, cols):
        show_room(col, img, rotator)


if check_password():
    st.set_page_config(layout="wide")

    frame = st.columns([1, 11, 1])[1]
    with frame:
        main()
