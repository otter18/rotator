# Rotator

## CLI

Для одного файла

```bash
python3 main.py static/img/90/doc.png
```

Рекурсивно для всех файлов в подпапках

```bash
python3 main.py -r static/img/90
```

Вывод в формате json

```bash
python3 main.py -r static/img/90 --json
```

## Demo

Доступен демо сайт, где можно проверить решения на своих картинках

![](demo.png)

## R&D

Исходные эксперименты проведены в ноутбуках:

- `cv2` - [rnd/cv2.ipynb](rnd/cv2.ipynb)
- `tesseract` - [rnd/tesseract.ipynb](rnd/tesseract.ipynb)