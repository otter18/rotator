import argparse
import json
import os
import sys

import cv2
from termcolor import cprint
from tqdm import tqdm

from rotators import Cv2Rotator, ProjectionRotator, TesseractRotator
from rotators import RotatorException

ALLOWED_EXTENSIONS = ['.png', '.jpeg', '.jpg']
ROTATORS = [TesseractRotator(), ProjectionRotator(), Cv2Rotator()]


def eval_file(path):
    img = cv2.imread(path)

    if img is None:
        cprint(f'Error while reading: {path}. Ignored.', 'yellow')
        return {'error': "can't read file"}

    res = {}
    for rotator in ROTATORS:
        try:
            res[rotator.name] = rotator.eval(img)
        except RotatorException as ex:
            res[rotator.name] = str(ex)
            cprint(f"Error while evaluating {path} on rotator '{rotator.name}': {ex}", 'yellow')

    return res


def parse_args():
    parser = argparse.ArgumentParser()

    parser.add_argument('path', help='Path to img or folder')
    parser.add_argument('-r', '--recursive', action='store_true', help='Evaluate all files in sub folders')
    parser.add_argument('--json', action='store_true', help='Output results in json format')

    return parser.parse_args()


def main():
    args = parse_args()

    if not os.path.exists(args.path):
        cprint(f'no such file or directory: {args.path}', 'red', file=sys.stderr)
        sys.exit(0)

    if args.recursive and os.path.isfile(args.path):
        cprint(f'{args.path} is file, not directory. Remove -r to run for single file', 'red', file=sys.stderr)
        sys.exit(0)

    imgs = []
    if args.recursive:
        for path, subdirs, files in os.walk(args.path):
            for name in files:
                if any(name.endswith(ext) for ext in ALLOWED_EXTENSIONS):
                    imgs.append(os.path.join(path, name))
    else:
        if any(args.path.endswith(ext) for ext in ALLOWED_EXTENSIONS):
            imgs.append(args.path)
        else:
            cprint(
                f"Files with extension '{args.path.split('.')[-1]}' are not allowed. Allowed: {', '.join(ALLOWED_EXTENSIONS)}",
                'red', file=sys.stderr
            )
            sys.exit(0)

    if not imgs:
        cprint(
            f"No files with allowed extensions found. Allowed: {', '.join(ALLOWED_EXTENSIONS)}",
            'red', file=sys.stderr
        )
        sys.exit(0)

    res = {}
    pbar = tqdm(imgs)
    for img_path in pbar:
        pbar.set_description(img_path)
        res[img_path] = eval_file(img_path)
        pbar.set_description()

    if args.json:
        print(json.dumps(res, indent=4))
    else:
        for path in imgs:
            cprint(f"{path}: {res[path]}", 'green')


if __name__ == '__main__':
    try:
        main()
    except Exception as ex:
        cprint(f'Unexpected error: {ex}', 'red', file=sys.stderr)
