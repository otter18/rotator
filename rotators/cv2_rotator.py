import math
import time

import cv2
import numpy as np

from .base import Rotator
from .exceptions import catch


class Cv2Rotator(Rotator):
    def __init__(self):
        super().__init__(
            'cv2.HoughLinesP',
            'cv2.HoughLinesP',
            '- Любой печатный текст обычно написан по строкам.\n'
            '- Найдем линии на картинке с помощью `cv2.HoughLinesP` (смотреть лог)\n'
            '- Работает только для 0/90 поворота, потому что направление строк таким способом найти не удается.\n'
            '- Работает быстро и не требует много ресурсов\n'
            '- Исходный эксперимент: https://gitlab.com/otter18/rotator/-/blob/main/rnd/cv2.ipynb\n'
        )

    @catch
    def eval(self, img, get_lines=False):
        gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
        edges = cv2.Canny(gray, 50, 150, apertureSize=3)
        lines = cv2.HoughLinesP(
            edges,
            1,  # Distance resolution in pixels
            np.pi / 180,  # Angle resolution in radians
            threshold=200,  # Min number of votes for valid line
            minLineLength=50,  # Min allowed length of line
            maxLineGap=100  # Max allowed gap between line for joining them
        )

        angles = []
        for points in lines:
            x1, y1, x2, y2 = points[0]
            angle = math.degrees(math.atan2(y2 - y1, x2 - x1))
            angles.append(angle)

        angle = (abs(round(np.median(angles))) + 45) // 90 * 90

        if get_lines:
            return angle, lines

        return angle

    def eval_meta(self, img):
        t0 = time.time()
        angle, lines = self.eval(img, get_lines=True)
        elapsed = time.time() - t0

        masked = img.copy()
        for points in lines:
            x1, y1, x2, y2 = points[0]
            cv2.line(masked, (x1, y1), (x2, y2), (0, 255, 0), 1)

        if angle != 0:
            rotated_img = cv2.rotate(img, self.ROTATE_TYPE[360 - angle])
        else:
            rotated_img = img.copy()

        return rotated_img, masked, angle, elapsed
