import cv2

from .exceptions import catch


class Rotator:
    ROTATE_TYPE = {
        90: cv2.ROTATE_90_CLOCKWISE,
        180: cv2.ROTATE_180,
        270: cv2.ROTATE_90_COUNTERCLOCKWISE
    }

    def __init__(self, name, md_name, description):
        self.name = name
        self.md_name = md_name
        self.description = description

    @catch
    def eval(self, img):
        """
        Rotates img and returns orientation
        :param img: original img
        """

    def eval_meta(self, img):
        """
        Rotates img and returns additional metadata
        :param img: original img
        """
