import time

import cv2
import pytesseract

from .base import Rotator
from .exceptions import catch


class TesseractRotator(Rotator):
    def __init__(self):
        super().__init__(
            'Tesseract',
            'Tesseract',
            '- Используется Tesseract OCR от гугла.\n'
            '- Метод `pytesseract.image_to_osd` среди прочего возвращает ориентацию документа (смотреть лог)\n'
            '- Работает относительно быстро, если учитывать распознавание текста под капотом\n'
            '- Исходный эксперимент: https://gitlab.com/otter18/rotator/-/blob/main/rnd/tesseract.ipynb\n'
        )

    @catch
    def eval(self, img, get_data=False):
        data = pytesseract.image_to_osd(img, output_type=pytesseract.Output.DICT)
        angle = data['orientation']

        if get_data:
            return angle, data

        return angle

    def eval_meta(self, img):
        t0 = time.time()
        angle, data = self.eval(img, get_data=True)
        elapsed = time.time() - t0

        rotated_img = img.copy()
        if angle != 0:
            rotated_img = cv2.rotate(img, self.ROTATE_TYPE[360 - angle])

        return rotated_img, data, angle, elapsed
