from functools import wraps


class RotatorException(Exception):
    pass


def catch(f=None, exception=RotatorException):
    """
    Decorator to wrap exceptions
    :param exception: target exception
    """

    def catch_decorator(f):
        @wraps(f)
        def func_with_retries(*args, **kwargs):
            try:
                return f(*args, **kwargs)
            except Exception as ex:
                raise RotatorException(ex)

        return func_with_retries

    if f and callable(f):
        return catch_decorator(f)
    else:
        def decorator(f):
            return catch_decorator(f)

        return decorator
