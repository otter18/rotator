import time

import cv2
import numpy as np
from matplotlib import pyplot as plt

from .base import Rotator
from .exceptions import catch


class ProjectionRotator(Rotator):
    def __init__(self):
        super().__init__(
            'Projection',
            'Projection :red[*NEW*]',
            '- Возьмем grayscale от картинки\n'
            '- Посмотри на проекции пикселей на горизонталь и вертикаль (смотреть лог)\n'
            '- Строки дают высокие пики с пустотой между ними, а в другой проекции пиков нет\n'
            '- Сравним медиану и среднее значение, чтобы найти проекцию с пиками\n'
            '- Этот способ работает очень быстро и не требует сложных вычислений\n'
            '- Исходный эксперимент: https://gitlab.com/otter18/rotator/-/blob/main/rnd/projection.ipynb\n'
        )

    @catch
    def eval(self, img, get_data=False):
        gray = 255 - cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)  # inverted grayscale img
        hor_proj = np.sum(gray, 1)
        ver_proj = np.sum(gray, 0)

        hor_spikes = np.mean(hor_proj) > np.median(hor_proj)
        ver_spikes = np.mean(ver_proj) > np.median(ver_proj)

        angle = 0
        if not hor_spikes and ver_spikes:
            angle = 90

        if get_data:
            return angle, (hor_proj, hor_spikes, ver_proj, ver_spikes)

        return angle

    def eval_meta(self, img):
        t0 = time.time()
        angle, (hor_proj, hor_spikes, ver_proj, ver_spikes) = self.eval(img, get_data=True)
        elapsed = time.time() - t0

        fig, axs = plt.subplots(2)

        axs[0].set_title(f'Horizontal proj (spikes - {hor_spikes})')
        axs[0].imshow(img)
        axs[0].barh(range(len(hor_proj)), hor_proj / hor_proj.max() * img.shape[1])

        axs[1].set_title(f'Vertical proj (spikes - {ver_spikes})')
        axs[1].imshow(img)
        axs[1].bar(range(len(ver_proj)), ver_proj / ver_proj.max() * img.shape[0])

        fig.tight_layout()

        rotated_img = img.copy()
        if angle != 0:
            rotated_img = cv2.rotate(img, self.ROTATE_TYPE[360 - angle])

        return rotated_img, fig, angle, elapsed
